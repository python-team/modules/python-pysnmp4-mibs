<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN"
"http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd" [

<!--

Process this file with an XSLT processor: `xsltproc \
-''-nonet /usr/share/sgml/docbook/stylesheet/xsl/nwalsh/\
manpages/docbook.xsl manpage.dbk'.  A manual page
<package>.<section> will be generated.  You may view the
manual page with: nroff -man <package>.<section> | less'.  A
typical entry in a Makefile or Makefile.am is:

DB2MAN=/usr/share/sgml/docbook/stylesheet/xsl/nwalsh/\
manpages/docbook.xsl
XP=xsltproc -''-nonet

manpage.1: manpage.dbk
        $(XP) $(DB2MAN) $<

The xsltproc binary is found in the xsltproc package.  The
XSL files are in docbook-xsl.  Please remember that if you
create the nroff version in one of the debian/rules file
targets (such as build), you will need to include xsltproc
and docbook-xsl in your Build-Depends control field.

-->

  <!-- Fill in your name for FIRSTNAME and SURNAME. -->
  <!ENTITY dhfirstname "<firstname>Jan</firstname>">
  <!ENTITY dhsurname   "<surname>Luebbe</surname>">

  <!-- Please adjust the date whenever revising the manpage. -->
  <!ENTITY dhdate      "<date>october 31, 2006</date>">

  <!-- SECTION should be 1-8, maybe w/ subsection other parameters are
       allowed: see man(7), man(1). -->
  <!ENTITY dhsection   "<manvolnum>1</manvolnum>">
  <!ENTITY dhemail     "<email>jluebbe@lasnet.de</email>">
  <!ENTITY dhusername  "Jan Luebbe">
  <!ENTITY dhucpackage "<refentrytitle>PYSNMP</refentrytitle>">
  <!ENTITY dhpackage   "rebuild-pysnmp-mibs">

  <!ENTITY debian      "<productname>Debian</productname>">
  <!ENTITY gnu         "<acronym>GNU</acronym>">
  <!ENTITY gpl         "&gnu; <acronym>GPL</acronym>">
]>

<!-- ====================================================================== -->
<refentry lang="en">
  <refentryinfo>
    <address>
      &dhemail;
    </address>
    
    <copyright>
      <year>2006</year>
      <holder>&dhusername;</holder>
    </copyright>
    &dhdate;
  </refentryinfo>

    <!-- ============================================================ -->
  <refmeta>
    &dhucpackage;

    &dhsection;
  </refmeta>

  <!-- ============================================================ -->
  <refnamediv>
    <refname>&dhpackage;</refname>

    <refpurpose>Rebuilds the mib modules for pysnmp from the original
       mib definitions.</refpurpose>
  </refnamediv>

  <!-- ============================================================ -->
  <refsynopsisdiv>
    <cmdsynopsis>
      <command>&dhpackage;</command>
      <arg><option>-s mib-text-dir</option></arg>
      <arg choice="plain"><replaceable>-d pysnmp-mib-rebuild-dir</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>

  <!-- ============================================================ -->
  <refsect1>
    <title>DESCRIPTION</title>

    <para>
      rebuild-pysnmp-mibs  is   used  to  compile   the  original  mib
      definitions     into    the     format    used     by    pysnmp.
      pysnmp-mib-rebuild-dir  must  contain   the  old  compiled  mibs
      (i.e. IF-MIB.py). rebuild-pysnmp-mibs will  then try to find the
      original  mib  definitions   and  recompile  them.   By  default
      rebuild-pysnmp-mibs     searches    in    /usr/local/share/snmp,
      /usr/local/share/mibs, /usr/share/snmp and /usr/share/mibs.
    </para>

    <para>
      It is possible  to specify another source directory  with the -s
      mib-text-dir  option.  This  directory  will  then  be  searched
      instead.
    </para>
  </refsect1>

  <!-- ============================================================ -->
  <refsect1>
    <title>AUTHORS</title>

    <para>
      This program was written by <personname>
	<firstname>Ilya</firstname> <surname>Etingof</surname>
	</personname><email>ilya@glas.net</email>
    </para>

    <para>
      This manual  page was written by &dhusername;  &dhemail; for the
      &debian;  system (but  may be  used by  others).   Permission is
      granted to  copy, distribute  and/or modify this  document under
      the terms  of the  &gnu; General Public  License, Version  2 any
      later version published by the Free Software Foundation.
    </para>

    <para>
      On Debian systems, the complete text of the &gnu;
      General Public License can be found in
      <filename>/usr/share/common-licenses/GPL</filename>.
    </para>
  </refsect1>
</refentry>



